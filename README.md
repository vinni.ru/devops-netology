# devops-netology
devops-10 student

HW-02
В репозиторий не попадут:
1. Все файлы и каталоги, которые содержатся в скрытых каталогах .terraform где-либо на диске
2. Файлы с расширением .tfstate или файлы, в названии которых встречается .tfstate.
3. Файл c названием crash.log
4. Файлы с расширением .tfvars
5. Файлы override.tf и override.tf.json, а также файлы, в названии которых содержатся _override.tf и _override.tf.json
6. Скрытый файл .terraformrc и файл terraform.rc
